# Splitter

1. Get

`go get bitbucket.org/mon3ita/splitter`

2. Example

```
package yourpackage

import 'bitbucket.org/mon3ita/splitter'

func yourFunction() {
    splitter.Split(filePath, rules)
}
```