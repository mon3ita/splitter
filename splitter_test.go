package splitter

import (
	"testing"

	"os"
	"path/filepath"
	"log"
)

var rules map[string]string

func makeDummyFiles() {
	path := "D:\\dummy" // change path

	rules = make(map[string]string)

	files := []string{
		"a", "b", "c",
		"d", "e", "f",
		"g", "h", "i",
		"j", "k", "l",
		"m", "n", "o",
		"p", "q", "r",
		"s", "t", "u",
		"v", "w", "x",
		"y", "z", 
	}

	os.Mkdir(path, os.FileMode(int(0777)))
	for _, file := range files {
		filePath := filepath.Join(path, file + ".txt")
		f, err := os.Create(filePath)

		if err != nil {
			log.Fatal(err)
		}
		f.Close()

		rules[file] = filepath.Join(path, file)
	}
}

func cleanFiles() {
	err := os.RemoveAll("D:\\dummy")

	if err != nil {
		log.Fatal(err)
	}
}

func TestSplit(t * testing.T) {
	makeDummyFiles()

	path := "D:\\dummy"
	Split(path, rules)

	folders := 0
	for _, v := range rules {
		file, err := os.Stat(v)
		if err != nil {
			cleanFiles()
			log.Fatal(err)
		}

		if file.IsDir() {
			folders++
		}
	}

	if folders != len(rules) {
		t.Errorf("Expected %d folders, but found %d", len(rules), folders)
	}

	cleanFiles()
}