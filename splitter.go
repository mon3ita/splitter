package splitter

import (
	"os"
	"log"

	"math"
	"strings"
	"path/filepath"

	"github.com/thoas/go-funk"
	"sync"
)

var m sync.Mutex

func applyRule(filtered []string, newLocation string, wg *sync.WaitGroup) {
	for i := 0; i < len(filtered); i++ {
		file := filtered[i]
		_, fileName := filepath.Split(file)

		m.Lock()
		os.Mkdir(newLocation, os.FileMode(int(0777)))
		location := filepath.Join(newLocation, fileName)
		os.Rename(file, location)
		m.Unlock()
	}

	wg.Done()
}

func moveFiles(path string, rules []string, apply []string, wg *sync.WaitGroup) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	files, err := file.Readdir(0)
	if err != nil {
		log.Fatal(err)
	}

	var fileNames []string
	for _, file := range files {
		fileNames = append(fileNames, file.Name())
	}

	var wgs sync.WaitGroup
	for i := 0; i < len(rules); i++ {
		filtered := funk.FilterString(fileNames, func(name string) bool {
			return strings.HasPrefix(name, rules[i])
		})

		wgs.Add(1)
		go applyRule(filtered, apply[i], &wgs)
	}

	wgs.Wait()
	wg.Done()
}

func Split(path string, by map[string]string) {
	ncor_ := math.Log2(float64(len(by)))
	ncor := int(math.Floor(ncor_))

	var rules []string
	var apply []string

	for k, v := range by {
		rules = append(rules, k)
		apply = append(apply, v)
	}

	var wg sync.WaitGroup

	j := 0
	split := len(by) / ncor
	for i := 0; i < ncor; i++ {
		wg.Add(1)
		go moveFiles(path, rules[j:split], apply[j:split], &wg)
		j = split
		split += split

		if split > len(rules) {
			split = len(rules)
		}
	}

	wg.Wait()
}